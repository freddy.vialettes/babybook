package com.freddy.babybook.repository;

import com.freddy.babybook.model.Activity;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.Order;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.testng.annotations.Test;

import java.util.Date;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@Transactional
@PropertySource("application-test.properties")
public class ActivityRepositoryTest {
    @Autowired
    private ActivityRepository activityRepository;

//    @Before
//    public void setUp() {
//        Activity activity1 = new Activity();
//        activity1.setName("activité 1");
//        activity1.setDate(new Date());
//        //activity1.setActivityType(ActivityType.ACTIVITIES);
//        activity1.setDescription("Ceci est une description");
//        activity1.setUserId(1);
//
//        savedActivity = activityRepository.save(activity1);
//    }

    @Test
    @Order(1)
    public void testInsertActivity() {
        // Given
        Activity activity = new Activity();
        activity.setName("Ceci est une activité de test");
        activity.setDate(new Date());
        activity.setActivityTypeId(1);
        activity.setDescription("Ceci est une description de test");
        activity.setUserId(1);

        // When
        Activity savedActivity = activityRepository.save(activity);

        // Then
        assertNotNull(savedActivity);
        assertEquals("Ceci est une activité de test", savedActivity.getName());
        assertEquals(1, savedActivity.getId());
    }

//    @Test
//    @Order(2)
//    public void testFindAll() {
//        List<Activity> activities = activityRepository.findAll();
//        assertEquals(1, activities.size());
//    }
//
//    @Test
//    @Order(3)
//    public void testDelete() {
//        activityRepository.delete(savedActivity);
//        assertEquals(0, activityRepository.findAll().size());
//    }
}

package com.freddy.babybook.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.freddy.babybook.model.Activity;
import com.freddy.babybook.service.ActivityService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.RequestEntity.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@SpringBootTest
//@AutoConfigureMockMvc
@WebMvcTest(ActivityController.class)
public class ActivityControllerTest {
//    @Autowired
//    ActivityController activityController;
    @MockBean
    private ActivityService activityService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    @Test
    public void testInsertActivity() throws Exception {

        Activity activity = new Activity();
        activity.setName("activité 1");
        activity.setDate(new Date());
        activity.setUserId(3);
        activity.setDescription("Ceci est une description");
        activity.setActivityTypeId(1);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/activity")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(activity));

        ResultMatcher resultStatus = MockMvcResultMatchers.status().isOk();

        JsonNode result = objectMapper.readTree(mockMvc.perform(requestBuilder)
                .andExpect(resultStatus)
                .andReturn().getResponse().getContentAsString());

        Assertions.assertEquals(result.get("name").asText(), "activité 1");
        Assertions.assertEquals(result.get("description").asText(), "Ceci est une description");
        Assertions.assertEquals(result.get("userId").asInt(), 3);
        Assertions.assertEquals(result.get("activityId").asInt(), 1);

    }
}

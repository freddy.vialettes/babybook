--
-- Déchargement des données de la table `activity`
--

INSERT INTO `activity` (`id`, `name`, `date`, `user_id`, `child_id`, `activity_type`, `description`) VALUES
(1, 'peinture', '2023-05-09 00:00:00', 1, 1, 'PAINT', ''),
(2, 'collage', '2023-05-09 00:00:00', 1, 1, 'ACTIVITIES', ''),
(10, 'test ajout', '2023-05-22 18:49:27', 1, 1, 'ACTIVITIES', ''),
(11, 'collage', '2023-05-09 02:00:00', 1, 1, 'ACTIVITIES', ''),
(12, 'collage', '2023-05-09 02:00:00', 1, 1, 'ACTIVITIES', ''),
(13, 'collage', '2023-05-09 02:00:00', 1, 1, 'ACTIVITIES', ''),
(14, 'collage', '2023-05-09 02:00:00', 1, 1, 'HEALTH', ''),
(18, 'collage', '2023-05-09 02:00:00', 1, 7, 'STORIES', ''),
(19, 'collage', '2023-05-09 02:00:00', 1, 7, 'PAINT', ''),
(63, 'qd', '2023-09-16 17:55:19', 1, 1, 'HEALTH', ''),
(64, 'ze', '2023-09-16 18:09:15', 1, 1, 'HEALTH', ''),
(65, 'collage', '2023-05-09 02:00:00', 1, 7, 'HEALTH', NULL),
(66, 'collage', '2023-05-09 02:00:00', 1, 7, 'HEALTH', NULL),
(143, 'qdqsd', '2023-09-18 16:45:53', 1, 1, 'HEALTH', ''),
(144, 'Test Mattouff', '2023-09-18 17:29:54', 1, 1, 'HEALTH', 'Mattouff t\'étouffe'),
(145, 'teset', '2023-09-18 17:33:43', 1, 1, 'HEALTH', '');

--
-- Déchargement des données de la table `child`
--

INSERT INTO `child` (`id`, `name`, `user_id`) VALUES
(3, 'Line', 1),
(1, 'Lucas', 1),
(7, 'Lucas', 3),
(2, 'Victoire', 1);

--
-- Déchargement des données de la table `comment`
--

INSERT INTO `comment` (`id`, `content`, `date`, `activity_id`, `user_id`) VALUES
(1, 'blah blah', '2023-05-09 00:00:00', 1, 1),
(2, 'test', '2023-05-09 19:25:26', 1, 1),
(3, 'tcheu', '2023-05-09 19:55:17', 1, 1),
(4, 'blah blah blah', '2023-05-09 02:00:00', 11, 1),
(5, 'blah blah blah', '2023-05-09 02:00:00', 11, 1),
(6, 'blah blah blah', '2023-05-09 02:00:00', 11, 1),
(7, 'blah blah blah', '2023-05-09 02:00:00', 11, 1),
(8, 'blah blah blah', '2023-05-09 02:00:00', 19, 1),
(20, 'qskjdhqsd', '2023-09-18 17:30:38', 144, 1);

--
-- Déchargement des données de la table `picture`
--

INSERT INTO `picture` (`id`, `path`, `activity_id`, `date_upload`, `user_id`) VALUES
(39, '1695048360961_carbon (3).png', 143, '2023-09-18 16:46:01', 1),
(40, '1695051014294_carbon (10).png', 144, '2023-09-18 17:30:14', 1);

-- --------------------------------------------------------

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `email`, `password`, `role`) VALUES
(1, 'Freddy', 'Vialettes', 'freddy.vialettes@gmail.com', '$2a$10$JtoegJfz7Qonh56VvHKJiOg1iMzcGncCC9M01SUWwEB665P6HvpQ2', 'ADMIN'),
(2, 'Sarah', 'Mousset', 'sarah.mousset@gmail.com', 'sarah', 'USER'),
(3, 'Natacha', 'Wozniak', 'natacha.wozniak@laposte.net', 'natacha', 'USER'),
(9, 'Natacha', 'Wozniak', 'blah@laposte.net', 'natacha', 'USER'),
(10, 'freddy', 'vialettes', 'blah@gmail.com', 'Vettioal!', 'USER'),
(12, 'freddy', 'vialettes', 'blah2@gmail.com', '$2a$10$7UBJ0jiRBDNQtxs0EnGSseJWlXU2GEgaZT6Hc0M71GhuCqORaarFu', 'USER');
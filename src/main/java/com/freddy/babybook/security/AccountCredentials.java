package com.freddy.babybook.security;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AccountCredentials {

    private String email;

    private String password;
}

package com.freddy.babybook.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import java.io.IOException;
import java.util.List;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private final String secret;

    public JWTAuthorizationFilter(AuthenticationManager pAuthenticationManager, String pSecret) {
        super(pAuthenticationManager);
        this.secret = pSecret;
    }

    protected void doFilterInternal(HttpServletRequest pHttpServletRequest,
                                    HttpServletResponse pHttpServletResponse,
                                    FilterChain pFilterChain) throws IOException, ServletException {
        String header = pHttpServletRequest.getHeaders(HttpHeaders.AUTHORIZATION).nextElement();
        if(header == null || !header.startsWith("Bearer")) {
            throw new JwtException("Header with Authorization not found");
        }

        UsernamePasswordAuthenticationToken authenticationToken = getAuthentication(pHttpServletRequest);
        if(authenticationToken == null) {
            throw new JwtException("Auth not valid");
        }

        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        pFilterChain.doFilter(pHttpServletRequest, pHttpServletResponse);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest pHttpServletRequest) {
        String token = pHttpServletRequest.getHeader(HttpHeaders.AUTHORIZATION)
                .replace("Bearer", "")
                .trim();
        Jws<Claims> claims = Jwts.parserBuilder()
                .setSigningKey(secret.getBytes())
                .build()
                .parseClaimsJws(token);
        String user = claims.getBody().getSubject();
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority((String) claims.getBody().get("role"));

        return new UsernamePasswordAuthenticationToken(user, null, List.of(authority));
    }
}

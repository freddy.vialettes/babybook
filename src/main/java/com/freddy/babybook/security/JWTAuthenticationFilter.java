package com.freddy.babybook.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.freddy.babybook.model.User;
import com.freddy.babybook.repository.UserRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import jakarta.servlet.FilterChain;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;

    private final String secret;

    private final ApplicationContext applicationContext;

    public JWTAuthenticationFilter(AuthenticationManager pAuthenticationManager, String pSecret, ApplicationContext pApplicationContext) {
        authenticationManager = pAuthenticationManager;
        secret = pSecret;
        applicationContext = pApplicationContext;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest pHttpServletRequest, HttpServletResponse pHttpServletResponse) {
        try {
            AccountCredentials accountCredentials = new ObjectMapper()
                    .readValue(pHttpServletRequest.getInputStream(), AccountCredentials.class);
            return getAuthentication(accountCredentials);
        } catch (IOException pIOException) {
            throw new RuntimeException(pIOException);
        }
    }
    private Authentication getAuthentication(AccountCredentials pAccountCredentials) {
        try {
            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(pAccountCredentials.getEmail(),
                            pAccountCredentials.getPassword(), new ArrayList<>())
            );
        } catch (AuthenticationException pAuthenticationException) {
            throw pAuthenticationException;
        }
    }
    @Override
    public void successfulAuthentication(HttpServletRequest pHttpServletRequest, HttpServletResponse pHttpServletResponse,
                                         FilterChain pFilterChain, Authentication pAuthentication) {
        ZonedDateTime expiration = LocalDateTime.now().plusHours(12).atZone(ZoneId.of("Europe/Paris"));
        UserRepository userRepository = applicationContext.getBean(UserRepository.class);
        UserDetails principal = (UserDetails) pAuthentication.getPrincipal();
        User usersByEmail = userRepository.findByEmail(principal.getUsername()).get();

        String token = Jwts.builder()
                .setSubject(usersByEmail.getEmail())
                .claim("firstname", usersByEmail.getFirstName())
                .claim("lastname", usersByEmail.getLastName())
                .claim("id", usersByEmail.getId())
                .claim("role", usersByEmail.getRole())
                .setExpiration(Date.from(expiration.toInstant()))
                .signWith(Keys.hmacShaKeyFor(secret.getBytes()), SignatureAlgorithm.HS256)
                .compact()
        ;

        pHttpServletResponse.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token);
    }

}

package com.freddy.babybook.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity(securedEnabled = true)
public class SecurityConfig {

    @Autowired
    private AuthenticationConfiguration authenticationConfiguration;
    @Autowired
    private ApplicationContext applicationContext;
    @Value("${secret}")
    private String secret;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

        http.cors().and().csrf().disable()
                        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.authorizeHttpRequests(authorize -> {
            try {
                authorize
                        .requestMatchers(HttpMethod.POST, "/login")
                        .permitAll()
                        .anyRequest().authenticated()
                        .and()
                        .addFilterBefore(new JwtExceptionHandlerFilter(), JWTAuthorizationFilter.class)
                        .addFilter(new JWTAuthorizationFilter(authenticationConfiguration.getAuthenticationManager(),
                                secret));
            } catch (Exception e){
                throw new RuntimeException(e.getMessage());
            }
        }
        );

        return http.build();
    }

    @Bean
    public WebSecurityCustomizer webSecurityCustomizer() {
        return (web) -> web.ignoring().requestMatchers("/login").requestMatchers(HttpMethod.POST,"/api/user");
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder(10);
    }

    @Bean
    public AuthenticationManager authenticationManager(HttpSecurity pHttpSecurity, BCryptPasswordEncoder pBCryptPasswordEncoder,
                           SecurityUsersDetailsService pSecurityUsersDetailsService) throws  Exception {
        return pHttpSecurity.getSharedObject(AuthenticationManagerBuilder.class)
                .userDetailsService(pSecurityUsersDetailsService)
                .passwordEncoder(pBCryptPasswordEncoder)
                .and()
                .build();
    }

    @Bean
    public FilterRegistrationBean<JWTAuthenticationFilter> loggingFilter() throws Exception {
        FilterRegistrationBean<JWTAuthenticationFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new JWTAuthenticationFilter(authenticationConfiguration.getAuthenticationManager(), secret, applicationContext));
        registrationBean.addUrlPatterns("/login");
        return registrationBean;
    }

}

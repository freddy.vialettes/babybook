package com.freddy.babybook.service;

import com.freddy.babybook.model.Child;
import com.freddy.babybook.model.dto.ChildDTO;
import com.freddy.babybook.repository.ChildRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ChildService {

    @Autowired
    private ChildRepository childRepository;

    @Autowired
    private UserService userService;

    public Child addChild(Child pChild) {
        return childRepository.save(pChild);
    }

    public void deleteChild(Integer childId) {
        childRepository.deleteById(childId);
    }

    public Child updateChild(Integer childId, Child pChild) throws EntityNotFoundException {
        Optional<Child> optionalChild = childRepository.findById(childId);

        if(optionalChild.isEmpty()) {
            throw new EntityNotFoundException("Child with ID %d not exist".formatted(childId));
        }

        Child child = optionalChild.get();

        if(pChild.getName() != null || pChild.getName().isEmpty()) {
            child.setName(pChild.getName());
        }

        return childRepository.save(child);
    }

    public ChildDTO getOneChildById(Integer childId) {
        Optional<Child> optionalChild = childRepository.findById(childId);

        if(optionalChild.isEmpty()) {
            throw new EntityNotFoundException("Child with ID %d not exist".formatted(childId));
        }

        Child child = optionalChild.get();
        ChildDTO childDTO = new ChildDTO();

        childDTO.setId(child.getId());
        childDTO.setName(child.getName());
        childDTO.setUser(userService.getUserById(child.getUserId()));

        return childDTO;
    }

    public List<ChildDTO> getAllChild() {

        List<Child> childList = childRepository.findAll();
        List<ChildDTO> childDTOList = new ArrayList<>();

        for(Child child : childList) {
            ChildDTO childDTO = new ChildDTO();

            childDTO.setId(child.getId());
            childDTO.setName(child.getName());
            childDTO.setUser(userService.getUserById(child.getUserId()));

            childDTOList.add(childDTO);
        }

        return childDTOList;
    }
}

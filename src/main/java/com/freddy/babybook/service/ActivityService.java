package com.freddy.babybook.service;

import com.freddy.babybook.model.Activity;
import com.freddy.babybook.model.dto.ActivityDTO;
import com.freddy.babybook.repository.ActivityRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ActivityService {
    @Autowired
    private ActivityRepository activityRepository;
    @Autowired
    private UserService userService;
    public Activity addActivity(Activity pActivity) {
        return activityRepository.save(pActivity);
    }

    public void deleteActivity(Integer activityId) {
        activityRepository.deleteById(activityId);
    }

    public Activity updateActivity(Integer activityId, Activity pActivity) throws EntityNotFoundException {
        Optional<Activity> optionalActivity = activityRepository.findById(activityId);

        if(optionalActivity.isEmpty()) {
            throw new EntityNotFoundException("Activity with ID %d not exist".formatted(activityId));
        }

        Activity activity = optionalActivity.get();

        if(pActivity.getName() != null || !pActivity.getName().isEmpty()) {
            activity.setName(pActivity.getName());
        }
        if(pActivity.getDescription() != null || !pActivity.getDescription().isEmpty()) {
            activity.setDescription(pActivity.getDescription());
        }

        activity.setDate(new Date());

        return activityRepository.save(activity);
    }

    public List<ActivityDTO> getAllActivityByDay(Date pDate) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(pDate);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date startOfDay = calendar.getTime();

        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        Date endOfDay = calendar.getTime();

        List<Activity> activityList = activityRepository.findByDateBetweenOrderByDateAsc(startOfDay, endOfDay);

        List<ActivityDTO> activityDTOList = new ArrayList<>();

        for(Activity activity : activityList) {
            ActivityDTO activityDTO = new ActivityDTO();

            activityDTO.setId(activity.getId());
            activityDTO.setName(activity.getName());
            activityDTO.setDate(activity.getDate());
            activityDTO.setUser(userService.getUserById(activity.getUserId()));
            activityDTO.setPictures(activity.getPictures());
            activityDTO.setComments(activity.getComments());
            activityDTO.setActivityType(activity.getActivityTypeId());
            activityDTO.setDescription(activity.getDescription());

            activityDTOList.add(activityDTO);
        }

        return activityDTOList;
    }
}

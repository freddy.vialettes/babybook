package com.freddy.babybook.service;

import com.freddy.babybook.model.ActivityType;
import com.freddy.babybook.repository.ActivityTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityTypeService {

    @Autowired
    private ActivityTypeRepository activityRepository;

    public List<ActivityType> getAll() {
        return activityRepository.findAll();
    }
}

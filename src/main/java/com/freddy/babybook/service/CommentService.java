package com.freddy.babybook.service;

import com.freddy.babybook.model.Comment;
import com.freddy.babybook.model.dto.CommentDTO;
import com.freddy.babybook.repository.CommentRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CommentService {

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserService userService;

    public Comment addComment(Integer activityId, Integer userId, String content) {

        Comment comment = new Comment();
        comment.setDate(new Date());
        comment.setContent(content);
        comment.setUser(userId);
        comment.setActivity(activityId);
        return commentRepository.save(comment);
    }

    public void deleteComment(Integer commentId) {
        commentRepository.deleteById(commentId);
    }

    public Comment updateComment(Integer commentId, Comment pComment) throws EntityNotFoundException {
        Optional<Comment> optionalComment = commentRepository.findById(commentId);

        if(optionalComment.isEmpty()) {
            throw new EntityNotFoundException("Comment with ID %d not exist".formatted(commentId));
        }

        Comment comment = optionalComment.get();

        if(pComment.getContent() != null || pComment.getContent().isEmpty()) {
            comment.setContent(pComment.getContent());
        }

        comment.setDate(new Date());

        return commentRepository.save(comment);
    }
    public CommentDTO getOneCommentById(Integer commentId) {
        Optional<Comment> optionalComment = commentRepository.findById(commentId);

        if(optionalComment.isEmpty()) {
            throw new EntityNotFoundException("Comment with ID %d not exist".formatted(commentId));
        }

        Comment comment = optionalComment.get();
        CommentDTO commentDTO = new CommentDTO();

        commentDTO.setId(comment.getId());
        commentDTO.setDate(comment.getDate());
        commentDTO.setContent(comment.getContent());
        commentDTO.setUser(userService.getUserById(comment.getUser()));
        commentDTO.setActivityId(comment.getActivity());

        return commentDTO;
    }

    public List<CommentDTO> getAllComment() {

        List<Comment> commentList = commentRepository.findAll();
        List<CommentDTO> commentDTOList = new ArrayList<>();

        for(Comment comment : commentList) {
            CommentDTO commentDTO = new CommentDTO();

            commentDTO.setId(comment.getId());
            commentDTO.setDate(comment.getDate());
            commentDTO.setContent(comment.getContent());
            commentDTO.setUser(userService.getUserById(comment.getUser()));
            commentDTO.setActivityId(comment.getActivity());

            commentDTOList.add(commentDTO);
        }

        return commentDTOList;
    }

    public List<CommentDTO> getCommentsByActivity(Integer pActivityId) {

        List<Comment> commentList = commentRepository.findAllByActivity(pActivityId);
        List<CommentDTO> commentDTOList = new ArrayList<>();
        commentDTOList = commentList.stream().map(comment -> {
            CommentDTO commentDTO = new CommentDTO();
            commentDTO.setActivityId(comment.getActivity());
            commentDTO.setId(comment.getId());
            commentDTO.setDate(new Date());
            commentDTO.setContent(comment.getContent());
            commentDTO.setUser(userService.getUserById(comment.getUser()));
            return commentDTO;
        }).collect(Collectors.toList());

        return commentDTOList;
    }
}

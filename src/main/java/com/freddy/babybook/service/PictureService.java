package com.freddy.babybook.service;

import com.freddy.babybook.model.Picture;
import com.freddy.babybook.model.dto.PictureDTO;
import com.freddy.babybook.repository.PictureRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Service
public class PictureService {

    @Autowired
    private PictureRepository pictureRepository;

    @Autowired
    private UserService userService;

    public Picture savePicture(MultipartFile file, Integer activityId, Integer userId) throws IOException {

        Path uploadPath = Paths.get("src/main/resources/pictures");
        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }

        String fileName = System.currentTimeMillis() + "_" + file.getOriginalFilename();
        Path filePath = uploadPath.resolve(fileName);
        Files.copy(file.getInputStream(), filePath);

        Picture picture = new Picture();
        picture.setPath(fileName);
        picture.setDateUpload(new Date());
        picture.setUserId(userId);
        picture.setActivityId(activityId);
        return pictureRepository.save(picture);
    }

    public void deletePicture(Integer pictureId) {
        pictureRepository.deleteById(pictureId);
    }

    public PictureDTO getOnePictureById(Integer pictureId) {
        Optional<Picture> optionalPicture = pictureRepository.findById(pictureId);

        if(optionalPicture.isEmpty()) {
            throw new EntityNotFoundException("Picture with ID %d not exist".formatted(pictureId));
        }

        Picture picture = optionalPicture.get();
        PictureDTO pictureDTO = new PictureDTO();

        pictureDTO.setId(picture.getId());
        pictureDTO.setPath(picture.getPath());
        pictureDTO.setDateUpload(picture.getDateUpload());
        pictureDTO.setActivityId(picture.getActivityId());
        pictureDTO.setUser(userService.getUserById(picture.getUserId()));

        return pictureDTO;
    }

    public String getPictureByActivity(Integer pActivityId) {
        Picture picture = pictureRepository.findByActivityId(pActivityId);
        return String.valueOf(Paths.get(picture.getPath()));
    }
}

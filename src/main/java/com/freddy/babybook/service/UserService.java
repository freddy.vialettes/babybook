package com.freddy.babybook.service;

import com.freddy.babybook.model.User;
import com.freddy.babybook.model.dto.UserDTO;
import com.freddy.babybook.repository.UserRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public User addUser(User pUser) {
        pUser.setPassword(bCryptPasswordEncoder.encode(pUser.getPassword()));
        return userRepository.save(pUser);
    }

    public void deleteUser(Integer userId) {
        Optional<User> optionalUser = userRepository.findById(userId);

        if(optionalUser.isEmpty()) {
            throw new EntityNotFoundException("User with ID %d does not exist".formatted(userId));
        }

        userRepository.deleteById(userId);
    }

    public UserDTO getUserById(Integer userId) {
        Optional<User> optionalUser = userRepository.findById(userId);

        if(optionalUser.isEmpty()) {
            throw new EntityNotFoundException("User with ID %d does not exist".formatted(userId));
        }

        User user = optionalUser.get();
        UserDTO userDTO = new UserDTO();

        userDTO.setId(user.getId());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setEmail(user.getEmail());

        return userDTO;
    }

    public User updateUser(Integer userId, User pUser) {
        Optional<User> optionalUsers = userRepository.findById(userId);

        if(optionalUsers.isEmpty()) {
            throw new EntityNotFoundException("User with ID %d does not exist".formatted(userId));
        }

        User user = optionalUsers.get();

        if(pUser.getFirstName() != null ) {
            user.setFirstName(pUser.getFirstName());
        }

        if(pUser.getLastName() != null ) {
            user.setLastName(pUser.getLastName());
        }

        if(pUser.getEmail() != null ) {
            user.setEmail(pUser.getEmail());
        }

        if(pUser.getPassword() != null ) {
            user.setPassword(pUser.getPassword());
        }

        user.setRole(pUser.getRole());

        return userRepository.save(user);

    }

    public List<UserDTO> getAllUser() {

        List<UserDTO> userDTOList = new ArrayList<>();
        List<User> userList = userRepository.findAll();

        for (User user: userList) {
            UserDTO userDTO = new UserDTO();

            userDTO.setId(user.getId());
            userDTO.setFirstName(user.getFirstName());
            userDTO.setLastName(user.getLastName());
            userDTO.setEmail(user.getEmail());

            userDTOList.add(userDTO);
        }

        return userDTOList;
    }

}

package com.freddy.babybook.model;

public enum Role {
    ADMIN,
    USER;
}

package com.freddy.babybook.model.dto;

import com.freddy.babybook.model.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ActivityDTO {

    private Integer id;

    private String name;

    private Date date;

    private String description;

    private UserDTO user;

    private Integer activityType;

    private List<Comment> comments;

    private List<Picture> pictures;
}

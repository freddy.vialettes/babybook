package com.freddy.babybook.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PictureDTO {

    private Integer id;

    private String path;

    private Date dateUpload;

    private Integer activityId;

    private UserDTO user;
}

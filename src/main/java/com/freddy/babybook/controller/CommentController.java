package com.freddy.babybook.controller;

import com.freddy.babybook.model.Comment;
import com.freddy.babybook.model.dto.CommentDTO;
import com.freddy.babybook.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @GetMapping("")
    public ResponseEntity<List<CommentDTO>> getAllComment() {
        List<CommentDTO> commentList = commentService.getAllComment();

        return ResponseEntity.ok(commentList);
    }

    @GetMapping("/{activityId}")
    public ResponseEntity<List<CommentDTO>> getCommentsByActivity(@PathVariable("activityId") Integer activityId) {
        List<CommentDTO> commentDTOList = commentService.getCommentsByActivity(activityId);

        return ResponseEntity.ok(commentDTOList);
    }

    @PostMapping("")
    public ResponseEntity<Comment> insertComment(@RequestParam("activityId") Integer activityId, @RequestParam("userId") Integer userId, @RequestParam("content") String content) {
        Comment comment = commentService.addComment(activityId, userId, content);
        return ResponseEntity.ok(comment);
    }

    @PutMapping("/{commentId}")
    public ResponseEntity<Comment> updateComment(@PathVariable("commentId") Integer commentId, @RequestBody Comment pComment) {
        return ResponseEntity.ok(commentService.updateComment(commentId, pComment));
    }

    @DeleteMapping("/{commentId}")
    public ResponseEntity<Comment> deleteComment(@PathVariable("commentId") Integer commentId) {
        commentService.deleteComment(commentId);

        return ResponseEntity.ok().build();
    }
}

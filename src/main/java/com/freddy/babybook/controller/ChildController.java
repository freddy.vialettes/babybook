package com.freddy.babybook.controller;

import com.freddy.babybook.model.Child;
import com.freddy.babybook.model.dto.ChildDTO;
import com.freddy.babybook.service.ChildService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/child")
public class ChildController {

    @Autowired
    private ChildService childService;

    @GetMapping("")
    public ResponseEntity<List<ChildDTO>> getAllChild() {
        List<ChildDTO> childList = childService.getAllChild();

        return ResponseEntity.ok(childList);
    }

    @GetMapping("/{childId}")
    public ResponseEntity<ChildDTO> getOneChildById(@PathVariable("childId") Integer childId) {
        ChildDTO child = childService.getOneChildById(childId);

        return ResponseEntity.ok(child);
    }

    @PostMapping("")
    public ResponseEntity<Child> insertChild(@RequestBody Child pChild) {
        return ResponseEntity.ok(childService.addChild(pChild));
    }

    @PutMapping("/{childId}")
    public ResponseEntity<Child> updateChild(@PathVariable("childId") Integer childId, @RequestBody Child pChild) {
        return ResponseEntity.ok(childService.updateChild(childId, pChild));
    }

    @DeleteMapping("/{childId}")
    public ResponseEntity<Child> deleteChild(@PathVariable("childId") Integer childId) {
        childService.deleteChild(childId);

        return ResponseEntity.ok().build();
    }
}

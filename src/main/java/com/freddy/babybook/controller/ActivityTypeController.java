package com.freddy.babybook.controller;

import com.freddy.babybook.model.ActivityType;
import com.freddy.babybook.service.ActivityTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/activity-type")
public class ActivityTypeController {

    @Autowired
    private ActivityTypeService activityTypeService;

    @GetMapping("")
    public ResponseEntity<List<ActivityType>> getAllActivityType() {
        List<ActivityType> activityTypeList = activityTypeService.getAll();

        return ResponseEntity.ok(activityTypeList);
    }
}

package com.freddy.babybook.controller;

import com.freddy.babybook.model.Picture;
import com.freddy.babybook.model.dto.PictureDTO;
import com.freddy.babybook.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/picture")
public class PictureController {

    @Autowired
    private PictureService pictureService;

    @GetMapping(value = "/{activityId}", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> getPictureByActivity(@PathVariable("activityId") Integer activityId) throws IOException {

        String imgPath = pictureService.getPictureByActivity(activityId);

        ClassPathResource imgFile = new ClassPathResource("pictures/" + imgPath);
        byte[] bytes = StreamUtils.copyToByteArray(imgFile.getInputStream());

        return ResponseEntity
                .ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(bytes);
    }

    @PostMapping("/upload")
    public ResponseEntity<Picture> uploadPicture(@RequestParam("activityId") Integer activityId, @RequestParam("userId") Integer userId, @RequestParam("file") MultipartFile file) throws IOException {
        Picture picture = pictureService.savePicture(file, activityId, userId);
        return ResponseEntity.ok(picture);
    }
}

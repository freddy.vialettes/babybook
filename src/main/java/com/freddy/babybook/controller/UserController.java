package com.freddy.babybook.controller;

import com.freddy.babybook.model.User;
import com.freddy.babybook.model.dto.UserDTO;
import com.freddy.babybook.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("")
    public ResponseEntity<List<UserDTO>> getAllUser() {
        List<UserDTO> userList = userService.getAllUser();

        return ResponseEntity.ok(userList);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<UserDTO> getOneUserById(@PathVariable("userId") Integer userId) {
        return ResponseEntity.ok(userService.getUserById(userId));
    }

    @PostMapping("")
    public ResponseEntity<User> insertUser(@RequestBody User pUser) {
        return ResponseEntity.ok(userService.addUser(pUser));
    }

    @PutMapping("/{userId}")
    public ResponseEntity<User> updateUser(@PathVariable("userId") Integer userId, @RequestBody User pUser) {
        return ResponseEntity.ok(userService.updateUser(userId, pUser));
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<User> deleteUser(@PathVariable("userId") Integer userId) {
        userService.deleteUser(userId);

        return ResponseEntity.ok().build();
    }
}

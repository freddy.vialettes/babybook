package com.freddy.babybook.controller;

import com.freddy.babybook.model.Activity;
import com.freddy.babybook.model.dto.ActivityDTO;
import com.freddy.babybook.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/activity")
public class ActivityController {

    @Autowired
    private ActivityService activityService;

    @PostMapping("")
    public ResponseEntity<Activity> insertActivity(
            @RequestBody Activity pActivity) {
        return ResponseEntity.ok(
                activityService.addActivity(pActivity)
        );
    }

    @GetMapping("/date")
    public ResponseEntity<List<ActivityDTO>> getAllActivityByDay(@RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date) {
        List<ActivityDTO> activityList = activityService.getAllActivityByDay(date);

        return ResponseEntity.ok(activityList);
    }


    @PutMapping("/{activityId}")
    public ResponseEntity<Activity> updateActivity(@PathVariable("activityId") Integer activityId, @RequestBody Activity pActivity) {
        return ResponseEntity.ok(activityService.updateActivity(activityId, pActivity));
    }

    @DeleteMapping("{activityId}")
    public ResponseEntity<Activity> deleteActivity(@PathVariable("activityId") Integer activityId) {
        activityService.deleteActivity(activityId);
        return ResponseEntity.ok().build();
    }
}

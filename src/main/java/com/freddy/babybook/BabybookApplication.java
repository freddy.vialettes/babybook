package com.freddy.babybook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class BabybookApplication {

	public static void main(String[] args) {
		SpringApplication.run(BabybookApplication.class, args);
	}

}

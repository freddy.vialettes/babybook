package com.freddy.babybook.repository;

import com.freddy.babybook.model.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ActivityRepository
        extends JpaRepository<Activity, Integer> {
    List<Activity> findByDateBetweenOrderByDateAsc(
            Date start, Date end);
}

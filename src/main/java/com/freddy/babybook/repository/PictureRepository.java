package com.freddy.babybook.repository;

import com.freddy.babybook.model.Picture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PictureRepository extends JpaRepository<Picture, Integer> {
    List<Picture> findAllByActivityId(Integer activity);

    Picture findByActivityId(Integer activity);
}

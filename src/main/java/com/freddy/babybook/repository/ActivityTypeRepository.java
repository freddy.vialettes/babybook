package com.freddy.babybook.repository;

import com.freddy.babybook.model.ActivityType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActivityTypeRepository extends JpaRepository<ActivityType, Integer> {
}
